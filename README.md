![输入图片说明](https://gitee.com/open-source-byte/source-mall/raw/master/doc/source-mall3.jpg)


## 前言

`友源商城`是一个基于spring boot、uniapp 完全开源的商城系统，项目致力于为中小企业打造一个完整、易于维护的开源的电商系统，采用现阶段流行技术实现。后台管理系统包含商品管理、分类管理、订单管理、地址管理、广告管理、轮播管理、运营管理、内容管理、统计报表、权限管理、设置等模块。

## 演示地址

- 官网地址：<https://sourcebyte.vip> 
- 后台地址：<http://boot.sourcebyte.vip>  
- 开发文档：<http://doc.sourcebyte.vip>
- 小程序

<img src="https://gitee.com/open-source-byte/source-mall/raw/master/doc/qrcode.jpg"  width="220px" alt="小程序演示"/>

## 部分截图

### 1. 后台截图
![输入图片说明](https://gitee.com/open-source-byte/source-mall/raw/master/doc/5.png)
![输入图片说明](https://gitee.com/open-source-byte/source-mall/raw/master/doc/6.png)

### 2. 移动端截图
<p align="center">
<img alt="logo" src="https://gitee.com/open-source-byte/source-mall/raw/master/doc/0.jpg" style="width:100px;">
<img alt="logo" src="https://gitee.com/open-source-byte/source-mall/raw/master/doc/01.png" style="width:100px;">

### 作者信息

1.  作者：詹Sir
2.  邮箱：261648947@qq.com

### QQ交流群
| 1群已满(851042670) | 2群已满(522723115) | 3群(862649072) |
| :------: | :------: | :------: |
| <img src="https://gitee.com/open-source-byte/source-vue/raw/master/doc/qq01.png" width="200px">| <img src="https://gitee.com/open-source-byte/source-vue/raw/master/doc/qq02.png" width="200px">| <img src="https://gitee.com/open-source-byte/source-vue/raw/master/doc/qq03.png" width="200px">| 

### 商用授权

如需商业使用，请联系作者授权